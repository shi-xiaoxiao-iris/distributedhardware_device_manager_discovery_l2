/*
 * Copyright (c) 2023-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mine_softbus_listener.h"

#include <dlfcn.h>
#include <mutex>
#include <pthread.h>
#include <securec.h>
#include <thread>
#include <unistd.h>
#include <condition_variable>
#include <list>
#include <vector>

#include "device_manager_service.h"
#include "dm_anonymous.h"
#include "dm_constants.h"
#include "dm_device_info.h"
#include "dm_log.h"
#include "parameter.h"
#include "system_ability_definition.h"
#include "softbus_listener.h"
#include "dm_crypto.h"
#include "openssl/bio.h"
#include "openssl/sha.h"
#include "openssl/evp.h"
#include "openssl/pem.h"
#ifdef MINE_HARMONY
#include "softbus_bus_center_ex.h"
#endif

namespace OHOS {
namespace DistributedHardware {
constexpr uint32_t DM_MAX_SCOPE_TLV_NUM = 3;
constexpr uint32_t DM_MAX_VERTEX_TLV_NUM = 6;
constexpr int32_t SHA256_OUT_DATA_LEN = 32;
constexpr int32_t DM_MAX_DEVICE_ALIAS_LEN = 65;
constexpr int32_t DM_MAX_DEVICE_UDID_LEN = 65;
constexpr int32_t DM_INVALID_DEVICE_NUMBER = -1;
constexpr int32_t DM_TLV_VERTEX_DATA_OFFSET = 2;
constexpr int32_t DM_TLV_SCOPE_DATA_OFFSET = 4;
constexpr int32_t DM_SEARCH_BROADCAST_MIN_LEN = 18;
constexpr const char* FIELD_DEVICE_MODE = "findDeviceMode";
constexpr const char* FIELD_TRUST_OPTIONS = "tructOptions";
constexpr const char* FIELD_FILTER_OPTIONS = "filterOptions";
constexpr const char* DEVICE_ALIAS = "persist.devicealias";
constexpr const char* DEVICE_NUMBER = "persist.devicenumber";
constexpr const char* DEVICE_DM_KEY = "DM";
constexpr char BROADCAST_VERSION = 1;
constexpr char FIND_ALL_DEVICE = 1;
constexpr char FIND_SCOPE_DEVICE = 2;
constexpr char FIND_VERTEX_DEVICE = 3;
constexpr char FIND_TRUST_DEVICE = 3;
constexpr char DEVICE_ALIAS_NUMBER = 1;
constexpr char DEVICE_TYPE_TYPE = 1;
constexpr char DEVICE_SN_TYPE = 2;
constexpr char DEVICE_UDID_TYPE = 3;
constexpr char FIND_NOTRUST_DEVICE = 2;
constexpr uint32_t DM_MAX_DEVICE_NUMBER = 100000000;

#ifdef MINE_HARMONY
static IDiscoveryPolicyCallback publishLNNCallback_ = {
    .PolicyReport = MineSoftbusListener::OnPublishDeviceFound
};
#endif

MineSoftbusListener::MineSoftbusListener()
{
    if (PublishDeviceDiscovery() != DM_OK) {
        LOGE("failed to publish device sn sha256 hash to softbus");
    }
    LOGI("MineSoftbusListener constructor");
}

MineSoftbusListener::~MineSoftbusListener()
{
    if (PublishDeviceDiscovery(false) != DM_OK) {
        LOGI("fail to unregister service public callback");
    }
    LOGI("MineSoftbusListener destructor");
}

int32_t MineSoftbusListener::RefreshSoftbusLNN(const string &pkgName, const string &searchJson,
    const DmSubscribeInfo &dmSubscribeInfo)
{
    LOGI("start to start discovery device with pkgName: %{public}s", pkgName.c_str());
    size_t outLen = 0;
    char output[DISC_MAX_CUST_DATA_LEN] = {0};
    if (ParseSearchJson(pkgName, searchJson, output, &outLen) != DM_OK) {
        LOGE("failed to parse searchJson with pkgName: %{public}s", pkgName.c_str());
        return ERR_DM_JSON_PARSE_STRING;
    }
    SubscribeInfo subscribeInfo;
    SetSubscribeInfo(dmSubscribeInfo, subscribeInfo);
    if (SendBroadcastInfo(pkgName, subscribeInfo, output, outLen) != DM_OK) {
        LOGE("failed to start quick discovery beause sending broadcast info.");
        return ERR_DM_SOFTBUS_SEND_BROADCAST;
    }
    LOGI("start discovery device successfully with pkgName: %{public}s", pkgName.c_str());
    return DM_OK;
}

int32_t MineSoftbusListener::StopRefreshSoftbusLNN(uint16_t subscribeId)
{
    int retValue = StopRefreshLNN(DM_PKG_NAME, subscribeId);
    if (retValue != SOFTBUS_OK) {
        LOGE("failed to stop discovery device with ret: %{public}d", retValue);
        return ERR_DM_SOFTBUS_DISCOVERY_DEVICE;
    }
    return DM_OK;
}

void MineSoftbusListener::OnPublishDeviceFound(const DeviceInfo *deviceInfo)
{
    if (deviceInfo == nullptr) {
        LOGE("deviceInfo is nullptr.");
        return;
    }
    size_t custDataLen = strlen(deviceInfo->custData);
    if (custDataLen <= 0) {
        LOGE("failed to get broadcast data.");
        return;
    }
    LOGI("broadcast data is received with DataLen: %{public}u.", custDataLen);
    std::string businessDataStr = DecodeBroadcastData(deviceInfo->custData);
    if (businessDataStr.empty()) {
        LOGE("failed to decode broadcast data.");
        return;
    }
    if (businessDataStr.size() < DM_SEARCH_BROADCAST_MIN_LEN) {
        LOGE("deviceInfo data is short with dataLen.");
        return;
    }
    DeviceInfo tempDeviceInfo = *deviceInfo;
    std::thread dealTask(MatchSearchDealTask, tempDeviceInfo);
    dealTask.detach();
}

int32_t MineSoftbusListener::ParseSearchJson(const string &pkgName, const string &searchJson, char *output,
    size_t *outLen)
{
    cJSON *root = cJSON_Parse(searchJson.c_str());
    if (root == NULL) {
        LOGE("failed to parse filter options string.");
        return ERR_DM_INVALID_JSON_STRING;
    }
    cJSON *object = cJSON_GetObjectItem(root, FIELD_DEVICE_MODE);
    if (object == NULL || !cJSON_IsNumber(object)) {
        LOGE("key is not exist or is not string with key: %{public}s.", FIELD_DEVICE_MODE);
        cJSON_Delete(root);
        return ERR_DM_FAILED;
    }
    int32_t retValue = ERR_DM_FAILED;
    uint32_t findMode = object->valueint;
    LOGI("quick search device mode is: %{public}u", findMode);
    switch (findMode) {
        case FIND_ALL_DEVICE:
            retValue = ParseSearchAllDevice(root, pkgName, output, outLen);
            break;
        case FIND_SCOPE_DEVICE:
            retValue = ParseSearchScopeDevice(root, pkgName, output, outLen);
            break;
        case FIND_VERTEX_DEVICE:
            retValue = ParseSearchVertexDevice(root, pkgName, output, outLen);
            break;
        default:
            LOGE("key type is not match key: %{public}s.", FIELD_DEVICE_MODE);
    }
    cJSON_Delete(root);
    if (retValue != DM_OK) {
        LOGE("fail to parse search find device with ret: %{public}d.", retValue);
        return retValue;
    }
    LOGI("parse search json successfully with pkgName: %{public}s, outLen: %{public}zu,", pkgName.c_str(), *outLen);
    return DM_OK;
}

int32_t MineSoftbusListener::ParseSearchAllDevice(const cJSON *object, const string &pkgName, char *output,
    size_t *outLen)
{
    BroadcastHead broadcastHead;
    if (SetBroadcastHead(object, pkgName, broadcastHead) != DM_OK) {
        LOGE("fail to set broadcast head.");
        return ERR_DM_FAILED;
    }
    broadcastHead.tlvDataLen = 0;
    broadcastHead.findMode = FIND_ALL_DEVICE;
    AddHeadToBroadcast(broadcastHead, output);
    *outLen = sizeof(BroadcastHead);
    return DM_OK;
}

int32_t MineSoftbusListener::ParseSearchScopeDevice(const cJSON *root, const string &pkgName, char *output,
    size_t *outLen)
{
    BroadcastHead broadcastHead;
    if (SetBroadcastHead(root, pkgName, broadcastHead) != DM_OK) {
        LOGE("fail to set broadcast head.");
        return ERR_DM_FAILED;
    }
    cJSON *object = cJSON_GetObjectItem(root, FIELD_FILTER_OPTIONS);
    if (object == NULL || !cJSON_IsArray(object)) {
        LOGE("failed to get %{public}s scope cjson object or is not array.", FIELD_FILTER_OPTIONS);
        return ERR_DM_FAILED;
    }
    int arraySize = cJSON_GetArraySize(object);
    if (arraySize <= 0 || arraySize > DM_MAX_SCOPE_TLV_NUM) {
        LOGE("failed to get scope array cjson size or too lenght with len: %{public}d.", arraySize);
        return ERR_DM_INVALID_JSON_STRING;
    }
    LOGI("start to parse scope search array json with size:%{public}d.", arraySize);
    if (ParseScopeDeviceJsonArray(object, arraySize, output + sizeof(BroadcastHead), outLen) != DM_OK) {
        LOGE("failed to parse scope json array.");
        return ERR_DM_FAILED;
    }

    broadcastHead.findMode = FIND_SCOPE_DEVICE;
    broadcastHead.tlvDataLen = *outLen;
    AddHeadToBroadcast(broadcastHead, output);
    *outLen = *outLen + sizeof(BroadcastHead);
    return DM_OK;
}

int32_t MineSoftbusListener::ParseSearchVertexDevice(const cJSON *root, const string &pkgName, char *output,
    size_t *outLen)
{
    BroadcastHead broadcastHead;
    if (SetBroadcastHead(root, pkgName, broadcastHead) != DM_OK) {
        LOGE("fail to set broadcast head.");
        return ERR_DM_FAILED;
    }

    cJSON *object = cJSON_GetObjectItem(root, FIELD_FILTER_OPTIONS);
    if (object == NULL || !cJSON_IsArray(object)) {
        LOGE("failed to get %{public}s vertex cjson object or is not array.", FIELD_FILTER_OPTIONS);
        return ERR_DM_FAILED;
    }
    int arraySize = cJSON_GetArraySize(object);
    if (arraySize <= 0 || arraySize > DM_MAX_VERTEX_TLV_NUM) {
        LOGE("failed to get vertex array cjson size or too lenght with len: %{public}d.", arraySize);
        return ERR_DM_FAILED;
    }

    LOGI("start to parse vertex search array json with size: %{public}d.", arraySize);
    if (ParseVertexDeviceJsonArray(object, arraySize, output + sizeof(BroadcastHead), outLen) != DM_OK) {
        LOGE("failed to parse vertex json array.");
        return ERR_DM_FAILED;
    }

    broadcastHead.findMode = FIND_VERTEX_DEVICE;
    broadcastHead.tlvDataLen = *outLen;
    AddHeadToBroadcast(broadcastHead, output);
    *outLen = *outLen + sizeof(BroadcastHead);
    return DM_OK;
}

int32_t MineSoftbusListener::SetBroadcastHead(const cJSON *object, const string &pkgName, BroadcastHead &broadcastHead)
{
    broadcastHead.version = BROADCAST_VERSION;
    broadcastHead.headDataLen = sizeof(BroadcastHead);
    broadcastHead.tlvDataLen = 0;
    broadcastHead.findMode = 0;
    if (SetBroadcastTrustOptions(object, broadcastHead) != DM_OK) {
        LOGE("fail to set trust options to search broadcast.");
        return ERR_DM_FAILED;
    }
    if (SetBroadcastPkgname(pkgName, broadcastHead) != DM_OK) {
        LOGE("fail to set pkgname to search broadcast.");
        return ERR_DM_FAILED;
    }
    return DM_OK;
}

void MineSoftbusListener::AddHeadToBroadcast(const BroadcastHead &broadcastHead, char *output)
{
    size_t startPos = 0;
    output[startPos++] = broadcastHead.version;
    output[startPos++] = broadcastHead.headDataLen;
    output[startPos++] = broadcastHead.tlvDataLen;
    for (size_t i = 0; i < DM_HASH_DATA_LEN; i++) {
        output[startPos++] = broadcastHead.pkgNameHash[i];
    }
    output[startPos++] = broadcastHead.findMode;
    output[startPos++] = broadcastHead.trustFilter;
    LOGI("find device info with version: %{public}d, findMode: %{public}d, HeadLen: %{public}d,"
         "tlvDataLen: %{public}d, trustFilter: %{public}d", (int)(broadcastHead.version),
         (int)(broadcastHead.findMode), (int)(broadcastHead.headDataLen), (int)(broadcastHead.tlvDataLen),
         (int)(broadcastHead.trustFilter));
}

int32_t MineSoftbusListener::ParseScopeDeviceJsonArray(const cJSON *object, const int arraySize,
    char *output, size_t *outLen)
{
    char sha256Out[SHA256_OUT_DATA_LEN] = {0};

    for (int i = 0; i < arraySize; i++) {
        cJSON *item = cJSON_GetArrayItem(object, i);
        if (item == NULL) {
            LOGE("failed to get item with index: %{public}d.", i);
            return ERR_DM_FAILED;
        }
        cJSON *deviceAlias = cJSON_GetObjectItem(item, "deviceAlias");
        cJSON *startNumber = cJSON_GetObjectItem(item, "startNumber");
        cJSON *endNumber = cJSON_GetObjectItem(item, "endNumber");
        if (deviceAlias == NULL || startNumber == NULL || endNumber == NULL ||
            !cJSON_IsString(deviceAlias) || !cJSON_IsNumber(startNumber) || !cJSON_IsNumber(endNumber)) {
            LOGE("failed to get key or value with index: %{public}d.", i);
            return ERR_DM_FAILED;
        }
        if (startNumber->valueint >= DM_MAX_DEVICE_NUMBER || endNumber->valueint >= DM_MAX_DEVICE_NUMBER ||
            startNumber->valueint > endNumber->valueint) {
            LOGE("device number is out of the proper range with index: %{public}d, startNumber: %{public}d,"
                 "endNumber: %{public}d.", i, startNumber->valueint, endNumber->valueint);
            return ERR_DM_FAILED;
        }
        if (GetSha256Hash((const char*)deviceAlias->valuestring,
            strlen(deviceAlias->valuestring), sha256Out) != DM_OK) {
            LOGE("failed to get sha256 hash with index: %{public}d, value: %{public}s.", i, deviceAlias->valuestring);
            return ERR_DM_FAILED;
        }
        output[(*outLen)++] = DEVICE_ALIAS_NUMBER;
        output[(*outLen)++] = DM_HASH_DATA_LEN;
        output[(*outLen)++] = DM_DEVICE_NUMBER_LEN;
        output[(*outLen)++] = DM_DEVICE_NUMBER_LEN;
        for (int j = 0; j < DM_HASH_DATA_LEN; j++) {
            output[(*outLen)++] = sha256Out[j];
        }
        errno_t retValue = sprintf_s(&output[*outLen], DM_DEVICE_NUMBER_LEN, "%010d", startNumber->valueint);
        if (retValue == EOK) {
            LOGE("fail to add device number to data buffer");
            return ERR_DM_FAILED;
        }
        *outLen = *outLen + DM_DEVICE_NUMBER_LEN;
        retValue = sprintf_s(&output[*outLen], DM_DEVICE_NUMBER_LEN, "%010d", endNumber->valueint);
        if (retValue == EOK) {
            LOGE("fail to add device number to data buffer.");
            return ERR_DM_FAILED;
        }
        *outLen = *outLen + DM_DEVICE_NUMBER_LEN;
    }
    return DM_OK;
}

int32_t MineSoftbusListener::ParseVertexDeviceJsonArray(const cJSON *object, const int arraySize,
    char *output, size_t *outLen)
{
    char sha256Out[SHA256_OUT_DATA_LEN] = {0};

    for (int i = 0; i < arraySize; i++) {
        cJSON *item = cJSON_GetArrayItem(object, i);
        if (item == NULL) {
            LOGE("failed to get item with index: %{public}d.", i);
            return ERR_DM_POINT_NULL;
        }
        cJSON *type = cJSON_GetObjectItem(item, "type");
        cJSON *value = cJSON_GetObjectItem(item, "value");
        if (type == NULL || value == NULL || !cJSON_IsString(type) || !cJSON_IsString(value)) {
            LOGE("failed to get type or value cjson object with index: %{public}d.", i);
            return ERR_DM_POINT_NULL;
        }
        if (strcmp(type->valuestring, "deviceType") == 0) {
            output[(*outLen)++] = DEVICE_TYPE_TYPE;
        } else if (strcmp(type->valuestring, "deviceSn") == 0) {
            output[(*outLen)++] = DEVICE_SN_TYPE;
        } else if (strcmp(type->valuestring, "deviceUdid") == 0) {
            output[(*outLen)++] = DEVICE_UDID_TYPE;
        } else {
            LOGE("the value of type is not allowed with index: %{public}d, value: %{public}s.",
                i, value->valuestring);
            return ERR_DM_FAILED;
        }
        output[(*outLen)++] = DM_HASH_DATA_LEN;
        if (GetSha256Hash((const char*)value->valuestring, strlen(value->valuestring), sha256Out) != DM_OK) {
            LOGE("failed to get sha256 hash with index: %{public}d, value: %{public}s.", i, value->valuestring);
            return ERR_DM_GET_DATA_SHA256_HASH;
        }
        for (int j = 0; j < (int32_t)DM_HASH_DATA_LEN; j++) {
            output[(*outLen)++] = sha256Out[j];
        }
    }
    return DM_OK;
}

int32_t MineSoftbusListener::GetSha256Hash(const char *data, size_t len, char *output)
{
    if (data == nullptr || output == nullptr || len == 0) {
        LOGE("Input param invalied.");
        return ERR_DM_INPUT_PARA_INVALID;
    }
    SHA256_CTX ctx;
    SHA256_Init(&ctx);
    SHA256_Update(&ctx, data, len);
    SHA256_Final((unsigned char *)output, &ctx);
    return DM_OK;
}

int32_t MineSoftbusListener::SetBroadcastTrustOptions(const cJSON *root, BroadcastHead &broadcastHead)
{
    cJSON *object = cJSON_GetObjectItem(root, FIELD_TRUST_OPTIONS);
    if (object == NULL) {
        broadcastHead.trustFilter = 0;
        return DM_OK;
    } else if (cJSON_IsBool(object) && object->type == cJSON_True) {
        broadcastHead.trustFilter = FIND_TRUST_DEVICE;
        return DM_OK;
    } else if (cJSON_IsBool(object) && object->type == cJSON_False) {
        broadcastHead.trustFilter = FIND_NOTRUST_DEVICE;
        return DM_OK;
    }
    LOGE("key type is error with key: %{public}s", FIELD_TRUST_OPTIONS);
    return ERR_DM_FAILED;
}

int32_t MineSoftbusListener::SetBroadcastPkgname(const string &pkgName, BroadcastHead &broadcastHead)
{
    char sha256Out[SHA256_OUT_DATA_LEN] = {0};
    if (GetSha256Hash((const char *)pkgName.c_str(), pkgName.size(), sha256Out) != DM_OK) {
        LOGE("failed to get search pkgName sha256 hash while search all device.");
        return ERR_DM_FAILED;
    }
    for (size_t i = 0; i < DM_HASH_DATA_LEN; i++) {
        broadcastHead.pkgNameHash[i] = sha256Out[i];
    }
    return DM_OK;
}

void MineSoftbusListener::SetSubscribeInfo(const DmSubscribeInfo &dmSubscribeInfo, SubscribeInfo &subscribeInfo)
{
    subscribeInfo.subscribeId = dmSubscribeInfo.subscribeId;
    subscribeInfo.mode = (DiscoverMode)dmSubscribeInfo.mode;
    subscribeInfo.medium = (ExchangeMedium)dmSubscribeInfo.medium;
    subscribeInfo.freq = (ExchangeFreq)dmSubscribeInfo.freq;
    subscribeInfo.isSameAccount = dmSubscribeInfo.isSameAccount;
    subscribeInfo.isWakeRemote = dmSubscribeInfo.isWakeRemote;
    subscribeInfo.capability = DM_CAPABILITY_OSD;
    subscribeInfo.capabilityData = nullptr;
    subscribeInfo.dataLen = 0;
}

int32_t MineSoftbusListener::SendBroadcastInfo(const string &pkgName, SubscribeInfo &subscribeInfo, char *output,
    size_t outputLen)
{
    std::string base64Out = DmBase64Encode(output, outputLen);
    if (base64Out.empty()) {
        LOGE("failed to get search data base64 encode type data.");
        return ERR_DM_FAILED;
    }
    std::string castData = EncodeBroadcastData(base64Out);
    if (castData.empty()) {
        LOGE("failed to get broadcast data.");
        return ERR_DM_FAILED;
    }
    subscribeInfo.capabilityData = (unsigned char *)castData.c_str();
    subscribeInfo.dataLen = castData.size();
    LOGI("send search broadcast info by softbus with dataLen: %{public}zu.",
        castData.size());
    IRefreshCallback softbusRefreshCallback_ = SoftbusListener::GetSoftbusRefreshCb();
    int retValue = RefreshLNN(DM_PKG_NAME, &subscribeInfo, &softbusRefreshCallback_);
    if (retValue != SOFTBUS_OK) {
        LOGE("failed to start to refresh quick discovery with ret: %{public}d.", retValue);
        return ERR_DM_FAILED;
    }
    LOGI("send search broadcast info by softbus successfully with pkgName: %{public}s.", pkgName.c_str());
    return DM_OK;
}

std::string MineSoftbusListener::EncodeBroadcastData(const std::string &srcData)
{
    cJSON *root = cJSON_CreateObject();
    if (root == NULL) {
        LOGE("failed to create cjson object.");
        return "";
    }
    if (cJSON_AddStringToObject(root, DEVICE_DM_KEY, srcData.c_str()) == NULL) {
        LOGE("failed to add true to object.");
        cJSON_Delete(root);
        return "";
    }
    char *rootJsonStr = cJSON_PrintUnformatted(root);
    if (rootJsonStr == NULL) {
        LOGE("cJSON_PrintUnformatted failed, rootJsonStr is null.");
        cJSON_Delete(root);
        return "";
    }
    std::string destData(rootJsonStr);
    cJSON_Delete(root);
    cJSON_free(rootJsonStr);
    return destData;
}

std::string MineSoftbusListener::DecodeBroadcastData(const std::string &srcData)
{
    cJSON *root = cJSON_Parse(srcData.c_str());
    if (root == nullptr) {
        LOGE("root is nullptr.");
        return "";
    }
    cJSON *object = cJSON_GetObjectItem(root, DEVICE_DM_KEY);
    if (!cJSON_IsString(object)) {
        LOGE("object is not string.");
        cJSON_Delete(root);
        return "";
    }
    char *value = cJSON_GetStringValue(object);
    if (value == nullptr) {
        LOGE("get policy data is nullptr.");
        cJSON_Delete(root);
        return "";
    }
    cJSON_Delete(root);
    return std::string(value);
}

std::string MineSoftbusListener::DmBase64Encode(const char *input, size_t inputLen)
{
    LOGI("MineSoftbusListener::DmBase64Encode");
    if (input == nullptr || inputLen <= 0) {
        LOGE("Input param invalied.");
        return "";
    }

    auto memBio = BIO_new(BIO_s_mem());
    if (memBio == nullptr) {
        LOGE("memBio is nullptr.");
        return "";
    }
    auto b64Bio = BIO_new(BIO_f_base64());
    if (b64Bio == nullptr) {
        BIO_free(memBio);
        LOGE("b64Bio is nullptr.");
        return "";
    }

    BIO_push(b64Bio, memBio);
    BIO_set_flags(b64Bio, BIO_FLAGS_BASE64_NO_NL);

    int ret = BIO_write(b64Bio, input, inputLen);
    if (ret <= 0) {
        BIO_free_all(b64Bio);
        LOGE("BIO_write is fail.");
        return "";
    }

    BIO_flush(b64Bio);
    BUF_MEM *outBase64Ptr = nullptr;
    BIO_get_mem_ptr(b64Bio, &outBase64Ptr);
    if (outBase64Ptr == nullptr) {
        BIO_free_all(b64Bio);
        LOGE("BIO_get_mem_ptr fail.");
        return "";
    }
    std::string encodeStr = std::string(outBase64Ptr->data, outBase64Ptr->length);
    BIO_free_all(b64Bio);
    return encodeStr;
}

bool MineSoftbusListener::DmBase64Decode(unsigned char* outData, size_t &outSize,
    const char *input, size_t inputLen)
{
    LOGI("MineSoftbusListener::DmBase64Decode");
    if (outData == nullptr || input == nullptr || inputLen <= 0) {
        LOGE("Input param invalied.");
        return false;
    }
    auto memBio = BIO_new_mem_buf(input, inputLen);
    if (memBio == nullptr) {
        LOGE("memBio is nullptr.");
        return false;
    }
    auto b64Bio = BIO_new(BIO_f_base64());
    if (b64Bio == nullptr) {
        BIO_free(memBio);
        LOGE("b64Bio is nullptr.");
        return false;
    }
    BIO_push(b64Bio, memBio);
    BIO_set_flags(b64Bio, BIO_FLAGS_BASE64_NO_NL);
    int readRet = BIO_read_ex(b64Bio, outData, inputLen, &outSize);
    if (readRet <= 0 || outSize <= 0) {
        BIO_free_all(b64Bio);
        LOGE("BIO_read_ex is fail.");
        return false;
    }
    BIO_free_all(b64Bio);
    return true;
}

int32_t MineSoftbusListener::PublishDeviceDiscovery(bool enable)
{
#ifdef MINE_HARMONY
    int retValue = EnableDiscoveryPolicy(DM_PKG_NAME, DM_CAPABILITY_OSD, enable, &publishLNNCallback_);
    if (retValue != SOFTBUS_OK) {
        LOGE("[softbus]failed to call softbus publishLNN function with ret: %{public}d.", retValue);
        return ERR_DM_SOFTBUS_PUBLISH_SERVICE;
    }
#endif
    return DM_OK;
}

void MineSoftbusListener::MatchSearchDealTask(DeviceInfo deviceInfo)
{
    LOGI("the match deal task has started to run.");
    if (ParseBroadcastInfo(deviceInfo) != DM_OK) {
        LOGE("failed to parse broadcast info.");
    }
}

int32_t MineSoftbusListener::ParseBroadcastInfo(DeviceInfo &deviceInfo)
{
    char output[DISC_MAX_CUST_DATA_LEN] = {0};
    if (!GetBroadcastData(deviceInfo, output, DISC_MAX_CUST_DATA_LEN)) {
        LOGE("fail to get broadcast data");
        return ERR_DM_FAILED;
    }
    DevicePolicyInfo devicePolicyInfo;
    Action matchResult = DISCOVERY_POLICY_REJECT_REPLY;
    BroadcastHead broadcastHead = *(BroadcastHead *)output;
    LOGI("parse device info with version: %{public}d, findMode: %{public}d, HeadLen: %{public}d, tlvDataLen:"
        "%{public}d, trustFilter: %{public}d", (int)(broadcastHead.version), (int)(broadcastHead.findMode),
        (int)(broadcastHead.headDataLen), (int)(broadcastHead.tlvDataLen), (int)broadcastHead.trustFilter);

    char findMode = broadcastHead.findMode;
    switch (findMode) {
        case FIND_ALL_DEVICE:
            matchResult = MatchSearchAllDevice(deviceInfo, broadcastHead);
            break;
        case FIND_SCOPE_DEVICE:
            GetScopeDevicePolicyInfo(devicePolicyInfo);
            matchResult = MatchSearchScopeDevice(deviceInfo, output + sizeof(BroadcastHead),
                devicePolicyInfo, broadcastHead);
            break;
        case FIND_VERTEX_DEVICE:
            GetVertexDevicePolicyInfo(devicePolicyInfo);
            matchResult = MatchSearchVertexDevice(deviceInfo, output + sizeof(BroadcastHead),
                devicePolicyInfo, broadcastHead);
            break;
        default:
            LOGE("key type is not match key: %{public}s.", FIELD_DEVICE_MODE);
            return ERR_DM_FAILED;
    }
    LOGI("parse broadcast info matchResult: %{public}d.", (int)matchResult);
    if (matchResult == DISCOVERY_POLICY_ALLOW_REPLY) {
        return SendReturnwave(deviceInfo, broadcastHead, matchResult);
    }
    return DM_OK;
}

bool MineSoftbusListener::GetBroadcastData(DeviceInfo &deviceInfo, char *output, size_t outLen)
{
    std::string dmBase64Out(deviceInfo.custData);
    std::string businessDataStr = DecodeBroadcastData(dmBase64Out);
    if (businessDataStr.empty()) {
        LOGE("failed to decode broadcast data.");
        return false;
    }
    size_t base64OutLen = 0;
    if (!DmBase64Decode((unsigned char*)output, base64OutLen, businessDataStr.c_str(), businessDataStr.size())) {
        LOGE("DmBase64Decode failed.");
        return false;
    }
    if (base64OutLen < DM_SEARCH_BROADCAST_MIN_LEN) {
        LOGE("data length too short with outLen: %{public}zu.", base64OutLen);
        return false;
    }
    BroadcastHead *broadcastHead = (BroadcastHead *)output;
    size_t hDataLen = broadcastHead->headDataLen;
    size_t tlvDataLen = broadcastHead->tlvDataLen;
    if (hDataLen >= DISC_MAX_CUST_DATA_LEN || tlvDataLen >= DISC_MAX_CUST_DATA_LEN ||
        (hDataLen + tlvDataLen) != base64OutLen) {
        LOGE("data lenght is not valid with: headDataLen: %{public}zu, tlvDataLen: %{public}zu, base64OutLen:"
            "%{public}zu.", hDataLen, tlvDataLen, base64OutLen);
        return false;
    }
    return true;
}

Action MineSoftbusListener::MatchSearchAllDevice(DeviceInfo &deviceInfo, const BroadcastHead &broadcastHead)
{
    if (broadcastHead.trustFilter == 0) {
        return DISCOVERY_POLICY_ALLOW_REPLY;
    }
    if ((deviceInfo.isOnline) && (broadcastHead.trustFilter == FIND_TRUST_DEVICE)) {
        return DISCOVERY_POLICY_ALLOW_REPLY;
    } else if ((deviceInfo.isOnline) && (broadcastHead.trustFilter == FIND_NOTRUST_DEVICE)) {
        return DISCOVERY_POLICY_REJECT_REPLY;
    } else if (!(deviceInfo.isOnline) && (broadcastHead.trustFilter == FIND_TRUST_DEVICE)) {
        return DISCOVERY_POLICY_REJECT_REPLY;
    } else {
        return DISCOVERY_POLICY_ALLOW_REPLY;
    }
}

void MineSoftbusListener::GetScopeDevicePolicyInfo(DevicePolicyInfo &devicePolicyInfo)
{
    devicePolicyInfo.numberValid = false;
    devicePolicyInfo.aliasHashValid = false;
    if (GetDeviceAliasHash(devicePolicyInfo.aliasHash)) {
        devicePolicyInfo.aliasHashValid = true;
    }
    if (GetDeviceNumber(devicePolicyInfo.number)) {
        devicePolicyInfo.numberValid = true;
    }
}

Action MineSoftbusListener::MatchSearchScopeDevice(DeviceInfo &deviceInfo, char *output,
    const DevicePolicyInfo &devicePolicyInfo, const BroadcastHead &broadcastHead)
{
    vector<int> matchItemNum(DM_MAX_SCOPE_TLV_NUM, 0);
    vector<int> matchItemResult(DM_MAX_SCOPE_TLV_NUM, 0);

    if ((deviceInfo.isOnline) && (broadcastHead.trustFilter == FIND_NOTRUST_DEVICE)) {
        return DISCOVERY_POLICY_REJECT_REPLY;
    } else if (!(deviceInfo.isOnline) && (broadcastHead.trustFilter == FIND_TRUST_DEVICE)) {
        return DISCOVERY_POLICY_REJECT_REPLY;
    }

    size_t tlvLen = broadcastHead.tlvDataLen;
    const size_t ONE_TLV_DATA_LEN = DM_TLV_SCOPE_DATA_OFFSET + DM_HASH_DATA_LEN +
        DM_DEVICE_NUMBER_LEN + DM_DEVICE_NUMBER_LEN;
    for (size_t i = 0; (i + ONE_TLV_DATA_LEN) <= tlvLen; i += ONE_TLV_DATA_LEN) {
        if (output[i] == DEVICE_ALIAS_NUMBER) {
            size_t dataPosition = i + DM_TLV_SCOPE_DATA_OFFSET;
            size_t startNumberPosition = dataPosition + DM_HASH_DATA_LEN;
            int startNumber = atoi(&output[startNumberPosition]);
            if (startNumber == 0) {
                LOGE("the value of start device number is not allowed");
                continue;
            }
            size_t endNumberPosition = startNumberPosition + DM_DEVICE_NUMBER_LEN;
            int endNumber = atoi(&output[endNumberPosition]);
            if (endNumber == 0) {
                LOGE("the value of end device number is not allowed.");
                continue;
            }
            matchItemNum[DEVICE_ALIAS_NUMBER] = 1;
            if (CheckDeviceAliasMatch(devicePolicyInfo, &output[dataPosition]) &&
                CheckDeviceNumberMatch(devicePolicyInfo, startNumber, endNumber)) {
                matchItemResult[DEVICE_ALIAS_NUMBER] = 1;
            }
        } else {
            LOGE("the value of type is not allowed with type: %{public}u.", output[i]);
            continue;
        }
    }
    return GetMatchResult(matchItemNum, matchItemResult);
}

void MineSoftbusListener::GetVertexDevicePolicyInfo(DevicePolicyInfo &devicePolicyInfo)
{
    devicePolicyInfo.snHashValid = false;
    devicePolicyInfo.typeHashValid = false;
    devicePolicyInfo.udidHashValid = false;
    if (GetDeviceSnHash(devicePolicyInfo.snHash)) {
        devicePolicyInfo.snHashValid = true;
    }
    if (GetDeviceUdidHash(devicePolicyInfo.udidHash)) {
        devicePolicyInfo.udidHashValid = true;
    }
    if (GetDeviceTypeHash(devicePolicyInfo.typeHash)) {
        devicePolicyInfo.typeHashValid = true;
    }
}

Action MineSoftbusListener::MatchSearchVertexDevice(DeviceInfo &deviceInfo, char *output,
    const DevicePolicyInfo &devicePolicyInfo, const BroadcastHead &broadcastHead)
{
    vector<int> matchItemNum(DM_MAX_VERTEX_TLV_NUM, 0);
    vector<int> matchItemResult(DM_MAX_VERTEX_TLV_NUM, 0);

    if ((deviceInfo.isOnline) && (broadcastHead.trustFilter == FIND_NOTRUST_DEVICE)) {
        return DISCOVERY_POLICY_REJECT_REPLY;
    } else if (!(deviceInfo.isOnline) && (broadcastHead.trustFilter == FIND_TRUST_DEVICE)) {
        return DISCOVERY_POLICY_REJECT_REPLY;
    }

    size_t tlvLen = broadcastHead.tlvDataLen;
    const size_t ONE_TLV_DATA_LEN = DM_TLV_VERTEX_DATA_OFFSET + DM_HASH_DATA_LEN;
    for (size_t i = 0; (i + ONE_TLV_DATA_LEN) <= tlvLen; i += ONE_TLV_DATA_LEN) {
        size_t dataPosition = 0;
        if (output[i] == DEVICE_TYPE_TYPE) {
            dataPosition = i + DM_TLV_VERTEX_DATA_OFFSET;
            matchItemNum[DEVICE_TYPE_TYPE] = 1;
            if (CheckDeviceTypeMatch(devicePolicyInfo, &output[dataPosition])) {
                matchItemResult[DEVICE_TYPE_TYPE] = 1;
            }
        } else if (output[i] == DEVICE_SN_TYPE) {
            dataPosition = i + DM_TLV_VERTEX_DATA_OFFSET;
            matchItemNum[DEVICE_SN_TYPE] = 1;
            if (CheckDeviceSnMatch(devicePolicyInfo, &output[dataPosition])) {
                matchItemResult[DEVICE_SN_TYPE] = 1;
            }
        } else if (output[i] == DEVICE_UDID_TYPE) {
            dataPosition = i + DM_TLV_VERTEX_DATA_OFFSET;
            matchItemNum[DEVICE_UDID_TYPE] = 1;
            if (CheckDeviceUdidMatch(devicePolicyInfo, &output[dataPosition])) {
                matchItemResult[DEVICE_UDID_TYPE] = 1;
            }
        } else {
            LOGE("the value of type is not allowed with type: %{public}u.", output[i]);
        }
    }
    return GetMatchResult(matchItemNum, matchItemResult);
}

int32_t MineSoftbusListener::SendReturnwave(DeviceInfo &deviceInfo, const BroadcastHead &broadcastHead,
    Action matchResult)
{
    size_t outLen = 0;
    char outData[DISC_MAX_CUST_DATA_LEN] = {0};
    outData[outLen++] = BROADCAST_VERSION;
    outData[outLen++] = sizeof(ReturnwaveHead);
    outData[outLen++] = false;
    for (int i = 0; i < DM_HASH_DATA_LEN; i++) {
        outData[outLen++] = broadcastHead.pkgNameHash[i];
    }
    std::string base64Data = DmBase64Encode(outData, outLen);
    if (base64Data.empty()) {
        LOGE("failed to get search data base64 encode type data.");
        return ERR_DM_FAILED;
    }
    std::string castData = EncodeBroadcastData(base64Data);
    if (castData.empty()) {
        LOGE("failed to get broadcast data.");
        return ERR_DM_FAILED;
    }
    if (strcpy_s(deviceInfo.custData, DISC_MAX_CUST_DATA_LEN, castData.c_str()) != 0) {
        LOGE("strcpy_s broadcast data failed.");
        return ERR_DM_FAILED;
    }
#ifdef MINE_HARMONY
    int32_t retValue = SetDiscoveryPolicy(DM_CAPABILITY_OSD, (DiscoveryPolicy)matchResult, &deviceInfo);
    if (retValue != SOFTBUS_OK) {
        LOGE("failed to set discovery policy with ret: %{public}d.", retValue);
    }
#endif
    LOGI("set discovery policy successfully with dataLen: %{public}zu.", castData.size());
    return DM_OK;
}

bool MineSoftbusListener::GetDeviceAliasHash(char *output)
{
    char deviceAlias[DM_MAX_DEVICE_ALIAS_LEN + 1] = {0};
    int32_t retValue = GetParameter(DEVICE_ALIAS, "not exist", deviceAlias, DM_MAX_DEVICE_ALIAS_LEN);
    if (retValue < 0 || strcmp((const char *)deviceAlias, "not exist") == 0) {
        LOGE("failed to get device alias from system parameter with ret: %{public}d.", retValue);
        return false;
    }
    char sha256Out[SHA256_OUT_DATA_LEN] = {0};
    if (GetSha256Hash((const char *)deviceAlias, strlen(deviceAlias), sha256Out) != DM_OK) {
        LOGE("failed to generated device alias sha256 hash.");
        return false;
    }
    for (size_t i = 0; i < DM_HASH_DATA_LEN; i++) {
        output[i] = sha256Out[i];
    }
    return true;
}

bool MineSoftbusListener::GetDeviceSnHash(char *output)
{
    const char *deviceSn = GetSerial();
    if (deviceSn == NULL) {
        LOGE("failed to get device sn from system parameter.");
        return false;
    }
    char sha256Out[SHA256_OUT_DATA_LEN] = {0};
    if (GetSha256Hash((const char *)deviceSn, strlen(deviceSn), sha256Out) != DM_OK) {
        LOGE("failed to generated device sn sha256 hash.");
        return false;
    }
    for (size_t i = 0; i < DM_HASH_DATA_LEN; i++) {
        output[i] = sha256Out[i];
    }
    return true;
}

bool MineSoftbusListener::GetDeviceUdidHash(char *output)
{
    char deviceUdid[DM_MAX_DEVICE_UDID_LEN + 1] = {0};
    int32_t retValue = GetDevUdid(deviceUdid, DM_MAX_DEVICE_UDID_LEN);
    if (retValue != 0) {
        LOGE("failed to get local device udid with ret: %{public}d.", retValue);
        return false;
    }
    char sha256Out[SHA256_OUT_DATA_LEN] = {0};
    if (GetSha256Hash((const char *)deviceUdid, strlen(deviceUdid), sha256Out) != DM_OK) {
        LOGE("failed to generated device udid sha256 hash.");
        return false;
    }
    for (size_t i = 0; i < DM_HASH_DATA_LEN; i++) {
        output[i] = sha256Out[i];
    }
    return true;
}

bool MineSoftbusListener::GetDeviceTypeHash(char *output)
{
    const char *deviceType = GetDeviceType();
    if (deviceType == NULL) {
        LOGE("failed to get device type from system parameter.");
        return false;
    }
    char sha256Out[SHA256_OUT_DATA_LEN] = {0};
    if (GetSha256Hash((const char *)deviceType, strlen(deviceType), sha256Out) != DM_OK) {
        LOGE("failed to generated device type sha256 hash.");
        return false;
    }
    for (size_t i = 0; i < DM_HASH_DATA_LEN; i++) {
        output[i] = sha256Out[i];
    }
    return true;
}

bool MineSoftbusListener::GetDeviceNumber(char *output)
{
    char deviceNumber[DM_DEVICE_NUMBER_LEN + 1] = {0};
    int32_t retValue = GetParameter(DEVICE_NUMBER, "not exist", deviceNumber, DM_DEVICE_NUMBER_LEN);
    if (retValue < 0 || strcmp((const char *)deviceNumber, "not exist") == 0) {
        LOGE("failed to get device number from system parameter with ret: %{public}d.", retValue);
        return false;
    }
    for (size_t i = 0; i < DM_DEVICE_NUMBER_LEN; i++) {
        output[i] = deviceNumber[i];
    }
    return true;
}

bool MineSoftbusListener::CheckDeviceAliasMatch(const DevicePolicyInfo &devicePolicyInfo, const char *data)
{
    if (!devicePolicyInfo.aliasHashValid) {
        LOGE("device alias is not valid");
        return false;
    }
    for (size_t i = 0; i < DM_HASH_DATA_LEN; i++) {
        if (data[i] != devicePolicyInfo.aliasHash[i]) {
            LOGI("device alias is not match.");
            return false;
        }
    }
    LOGI("device alias is match.");
    return true;
}

bool MineSoftbusListener::CheckDeviceNumberMatch(const DevicePolicyInfo &devicePolicyInfo,
    int32_t startNumber, int32_t endNumber)
{
    if (!devicePolicyInfo.numberValid) {
        LOGE("device number is not valid");
        return false;
    }
    if (startNumber <= DM_INVALID_DEVICE_NUMBER || endNumber <= DM_INVALID_DEVICE_NUMBER) {
        LOGI("device number is match");
        return true;
    }
    int deviceNumber = atoi((const char *)devicePolicyInfo.number);
    if (deviceNumber < startNumber || deviceNumber > endNumber) {
        LOGI("device number is not match.");
        return false;
    }
    LOGI("device number is match.");
    return true;
}

bool MineSoftbusListener::CheckDeviceSnMatch(const DevicePolicyInfo &devicePolicyInfo, const char *data)
{
    if (!devicePolicyInfo.snHashValid) {
        LOGE("device sn is not valid");
        return false;
    }
    for (size_t i = 0; i < DM_HASH_DATA_LEN; i++) {
        if (data[i] != devicePolicyInfo.snHash[i]) {
            LOGI("device sn is not match.");
            return false;
        }
    }
    LOGI("device sn is match.");
    return true;
}

bool MineSoftbusListener::CheckDeviceTypeMatch(const DevicePolicyInfo &devicePolicyInfo, const char *data)
{
    if (!devicePolicyInfo.typeHashValid) {
        LOGE("device type is not valid");
        return false;
    }
    for (size_t i = 0; i < DM_HASH_DATA_LEN; i++) {
        if (data[i] != devicePolicyInfo.typeHash[i]) {
            LOGI("device type is not match.");
            return false;
        }
    }
    LOGI("device type is match.");
    return true;
}

bool MineSoftbusListener::CheckDeviceUdidMatch(const DevicePolicyInfo &devicePolicyInfo, const char *data)
{
    if (!devicePolicyInfo.udidHashValid) {
        LOGE("device udid is not valid");
        return false;
    }
    for (size_t i = 0; i < DM_HASH_DATA_LEN; i++) {
        if (data[i] != devicePolicyInfo.udidHash[i]) {
            LOGI("device udid is not match.");
            return false;
        }
    }
    LOGI("device udid is match.");
    return true;
}

Action MineSoftbusListener::GetMatchResult(const vector<int> &matchItemNum, const vector<int> &matchItemResult)
{
    int matchItemSum = 0;
    int matchResultSum = 0;
    size_t matchItemNumLen = matchItemNum.size();
    size_t matchItemResultLen = matchItemResult.size();
    size_t minLen = (matchItemNumLen >= matchItemResultLen ? matchItemResultLen : matchItemNumLen);
    for (size_t i = 0; i < minLen; i++) {
        matchResultSum += matchItemResult[i];
        matchItemSum += matchItemNum[i];
    }
    if (matchItemSum == matchResultSum) {
        return DISCOVERY_POLICY_ALLOW_REPLY;
    } else {
        return DISCOVERY_POLICY_REJECT_REPLY;
    }
}
} // namespace DistributedHardware
} // namespace OHOS