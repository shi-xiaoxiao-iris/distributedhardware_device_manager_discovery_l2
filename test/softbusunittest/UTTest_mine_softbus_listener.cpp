/*
 * Copyright (c) 2022-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "UTTest_mine_softbus_listener.h"

#include <securec.h>
#include <unistd.h>
#include <cstdlib>
#include <thread>

#include "dm_anonymous.h"
#include "dm_constants.h"
#include "dm_device_info.h"
#include "dm_log.h"
#include "parameter.h"
#include "nlohmann/json.hpp"
#include "system_ability_definition.h"

namespace OHOS {
namespace DistributedHardware {
void MineSoftbusListenerTest::SetUp()
{
}
void MineSoftbusListenerTest::TearDown()
{
}
void MineSoftbusListenerTest::SetUpTestCase()
{
}
void MineSoftbusListenerTest::TearDownTestCase()
{
}

namespace {
std::shared_ptr<MineSoftbusListener> mineSoftbusListener = std::make_shared<MineSoftbusListener>();
constexpr int32_t SHA256_OUT_DATA_LEN = 32;

HWTEST_F(MineSoftbusListenerTest, EnBroadcastData_001, testing::ext::TestSize.Level0)
{
    std::string srcStr = "hello";
    std::string destStr;
    std::string srcStr2;
    destStr = MineSoftbusListener::EncodeBroadcastData(srcStr);
    srcStr2 = MineSoftbusListener::DecodeBroadcastData(destStr);
    EXPECT_EQ(srcStr, srcStr2);
}

HWTEST_F(MineSoftbusListenerTest, OnPublishDeviceFound_001, testing::ext::TestSize.Level0)
{
    std::string searchJsonStr2 = R"({"findDeviceMode":1})";
    char output[DISC_MAX_CUST_DATA_LEN] = {0};
    size_t outLen = 0;
    std::string pkgName = "ohos.test.hello";
    int32_t ret = mineSoftbusListener->ParseSearchJson(pkgName, searchJsonStr2, output, &outLen);
    printf("output: %s, outSize: %d\n", output, outLen);
    std::string base64Out = mineSoftbusListener->DmBase64Encode(output, outLen);
    std::string castData = mineSoftbusListener->EncodeBroadcastData(base64Out);
    printf("start parse...\n");
    DeviceInfo deviceInfo;
    if (strcpy_s(deviceInfo.custData, DISC_MAX_CUST_DATA_LEN, castData.c_str()) == 0) {
        printf("custData: %s\n", deviceInfo.custData);
        mineSoftbusListener->OnPublishDeviceFound(&deviceInfo);
    }
    EXPECT_EQ(ret, DM_OK);
}

HWTEST_F(MineSoftbusListenerTest, DmBase64Encode_001, testing::ext::TestSize.Level0)
{
    std::string input = "hellohellohellohello!";
    char output[DISC_MAX_CUST_DATA_LEN] = {0};
    size_t outLen = 0;
    std::string custData = MineSoftbusListener::DmBase64Encode(input.c_str(), input.size());
    MineSoftbusListener::DmBase64Decode((unsigned char*)output, outLen, custData.c_str(), custData.size());
    std::string output2(output, outLen);
    EXPECT_EQ(input, output2);
}

HWTEST_F(MineSoftbusListenerTest, DmBase64Encode_002, testing::ext::TestSize.Level0)
{
    std::string searchJsonStr2 = R"({"findDeviceMode":1})";
    char output[DISC_MAX_CUST_DATA_LEN] = {0};
    size_t outLen = 0;
    std::string pkgName = "ohos.test.hello";
    int32_t ret = mineSoftbusListener->ParseSearchJson(pkgName, searchJsonStr2, output, &outLen);
    BroadcastHead broadcastHead = *(BroadcastHead *)output;
    printf("parse device info with version: %d, findMode: %d, HeadLen: %d, tlvDataLen:"
        "%d, trustFilter: %d\n", (int)(broadcastHead.version), (int)(broadcastHead.findMode),
        (int)(broadcastHead.headDataLen), (int)(broadcastHead.tlvDataLen), (int)broadcastHead.trustFilter);
    std::string base64Out1 = MineSoftbusListener::DmBase64Encode(output, outLen);
    std::string dmBase64Out = MineSoftbusListener::EncodeBroadcastData(base64Out1);
    std::string base64Out2 = MineSoftbusListener::DecodeBroadcastData(dmBase64Out);
    char output2[DISC_MAX_CUST_DATA_LEN] = {0};
    size_t outputSize = 0;
    MineSoftbusListener::DmBase64Decode((unsigned char*)output2, outputSize, base64Out2.c_str(), base64Out2.size());
    BroadcastHead broadcastHead2 = *(BroadcastHead *)output2;
    printf("parse device info with version: %d, findMode: %d, HeadLen: %d, tlvDataLen:"
        "%d, trustFilter: %d\n", (int)(broadcastHead2.version), (int)(broadcastHead2.findMode),
        (int)(broadcastHead2.headDataLen), (int)(broadcastHead2.tlvDataLen), (int)broadcastHead2.trustFilter);
    EXPECT_EQ(ret, DM_OK);
}

HWTEST_F(MineSoftbusListenerTest, DmBase64Encode_003, testing::ext::TestSize.Level0)
{
    std::string searchJsonStr2 = R"({"findDeviceMode":2,"filterOptions":[
        {"deviceAlias":"bestechnic","startNumber":1, "endNumber":10}]})";
    char output[DISC_MAX_CUST_DATA_LEN] = {0};
    size_t outLen = 0;
    std::string pkgName = "ohos.test.hello";
    int32_t ret = mineSoftbusListener->ParseSearchJson(pkgName, searchJsonStr2, output, &outLen);
    BroadcastHead broadcastHead = *(BroadcastHead *)output;
    printf("parse device info with version: %d, findMode: %d, HeadLen: %d, tlvDataLen:"
        "%d, trustFilter: %d\n", (int)(broadcastHead.version), (int)(broadcastHead.findMode),
        (int)(broadcastHead.headDataLen), (int)(broadcastHead.tlvDataLen), (int)broadcastHead.trustFilter);
    std::string base64Out1 = MineSoftbusListener::DmBase64Encode(output, outLen);
    std::string dmBase64Out = MineSoftbusListener::EncodeBroadcastData(base64Out1);
    std::string base64Out2 = MineSoftbusListener::DecodeBroadcastData(dmBase64Out);
    char output2[DISC_MAX_CUST_DATA_LEN] = {0};
    size_t outputSize = 0;
    MineSoftbusListener::DmBase64Decode((unsigned char*)output2, outputSize, base64Out2.c_str(), base64Out2.size());
    BroadcastHead broadcastHead2 = *(BroadcastHead *)output2;
    printf("parse device info with version: %d, findMode: %d, HeadLen: %d, tlvDataLen:"
        "%d, trustFilter: %d\n", (int)(broadcastHead2.version), (int)(broadcastHead2.findMode),
        (int)(broadcastHead2.headDataLen), (int)(broadcastHead2.tlvDataLen), (int)broadcastHead2.trustFilter);
    EXPECT_EQ(ret, DM_OK);
}

HWTEST_F(MineSoftbusListenerTest, DmBase64Encode_004, testing::ext::TestSize.Level0)
{
    std::string searchJsonStr2 = R"({"findDeviceMode":3,"filterOptions":[
        {"type":"deviceSn","value":"12344556779888982345663345342312"}]})";
    char output[DISC_MAX_CUST_DATA_LEN] = {0};
    size_t outLen = 0;
    std::string pkgName = "ohos.test.hello";
    int32_t ret = mineSoftbusListener->ParseSearchJson(pkgName, searchJsonStr2, output, &outLen);
    BroadcastHead broadcastHead = *(BroadcastHead *)output;
    printf("parse device info with version: %d, findMode: %d, HeadLen: %d, tlvDataLen:"
        "%d, trustFilter: %d\n", (int)(broadcastHead.version), (int)(broadcastHead.findMode),
        (int)(broadcastHead.headDataLen), (int)(broadcastHead.tlvDataLen), (int)broadcastHead.trustFilter);
    std::string base64Out1 = MineSoftbusListener::DmBase64Encode(output, outLen);
    std::string dmBase64Out = MineSoftbusListener::EncodeBroadcastData(base64Out1);
    std::string base64Out2 = MineSoftbusListener::DecodeBroadcastData(dmBase64Out);
    char output2[DISC_MAX_CUST_DATA_LEN] = {0};
    size_t outputSize = 0;
    MineSoftbusListener::DmBase64Decode((unsigned char*)output2, outputSize, base64Out2.c_str(), base64Out2.size());
    BroadcastHead broadcastHead2 = *(BroadcastHead *)output2;
    printf("parse device info with version: %d, findMode: %d, HeadLen: %d, tlvDataLen:"
        "%d, trustFilter: %d\n", (int)(broadcastHead2.version), (int)(broadcastHead2.findMode),
        (int)(broadcastHead2.headDataLen), (int)(broadcastHead2.tlvDataLen), (int)broadcastHead2.trustFilter);
    EXPECT_EQ(ret, DM_OK);
}

HWTEST_F(MineSoftbusListenerTest, RefreshSoftbusLNN_001, testing::ext::TestSize.Level0)
{
    string pkgName = "ohos.test.hello";
    string searchJson = "{\"findDeviceMode\":1}";
    DmSubscribeInfo dmSubscribeInfo;
    int32_t ret = mineSoftbusListener->RefreshSoftbusLNN(pkgName, searchJson, dmSubscribeInfo);
    EXPECT_EQ(ret, ERR_DM_SOFTBUS_SEND_BROADCAST);
}

HWTEST_F(MineSoftbusListenerTest, SetBroadcastPkgname_001, testing::ext::TestSize.Level0)
{
    string pkgName = "ohos.test.hello";
    BroadcastHead broadcastHead;
    int32_t ret = mineSoftbusListener->SetBroadcastPkgname(pkgName, broadcastHead);
    EXPECT_EQ(ret, DM_OK);
}

HWTEST_F(MineSoftbusListenerTest, GetSha256Hash_001, testing::ext::TestSize.Level0)
{
    string pkgName = "ohos.test.hello";
    char sha256Out[SHA256_OUT_DATA_LEN] = {0};
    int32_t ret = mineSoftbusListener->GetSha256Hash(pkgName.c_str(), pkgName.size(), sha256Out);
    EXPECT_EQ(ret, DM_OK);
}

HWTEST_F(MineSoftbusListenerTest, GetDeviceAliasHash_001, testing::ext::TestSize.Level0)
{
    char sha256Out[SHA256_OUT_DATA_LEN] = {0};
    const char *deviceAliasKey = "persist.devicealias";
    const char *deviceAliasValue = "bestechnic";
    int ret = SetParameter(deviceAliasKey, deviceAliasValue);
    EXPECT_EQ(ret, 0);
    bool bRet = mineSoftbusListener->GetDeviceAliasHash(sha256Out);
    EXPECT_EQ(bRet, true);
}

HWTEST_F(MineSoftbusListenerTest, GetDeviceSnHash_001, testing::ext::TestSize.Level0)
{
    char sha256Out[SHA256_OUT_DATA_LEN] = {0};
    bool ret = mineSoftbusListener->GetDeviceSnHash(sha256Out);
    EXPECT_EQ(ret, true);
}

HWTEST_F(MineSoftbusListenerTest, GetDeviceUdidHash_001, testing::ext::TestSize.Level0)
{
    char sha256Out[SHA256_OUT_DATA_LEN] = {0};
    bool ret = mineSoftbusListener->GetDeviceUdidHash(sha256Out);
    EXPECT_EQ(ret, true);
}
} // namespace
} // namespace DistributedHardware
} // namespace OHOS